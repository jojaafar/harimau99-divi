## Overview 

To get these effects work you will need to copy CSS code snippets and paste them into _**Divi -> Theme Options -> General -> Custom CSS**_ or into child theme _**style.css**_ file.

General CSS Setup: [Click Here](https://gitlab.com/jojaafar/harimau99-divi/blob/master/h-divi/%5Bh-divi%5Dslidernavigation/%5Bh-divi%5Dslider-dot-navigation.css)


Then add the CSS class indicated for each effect below to **_Slider Module Settings -> Custom CSS -> CSS Class_**.

![inside slider module](https://www.divicio.us/wp-content/uploads/2016/08/add-css-class-here-3.jpg)


## Dot navigation effects CSS snippets

These are the dot navigation effects CSS snippets, copy and paste them into _**Divi -> Theme Options -> Custom CSS**_ field or **_style.css_** file.

 - 3D flipped effect : [Click Here](https://gitlab.com/jojaafar/harimau99-divi/blob/master/h-divi/%5Bh-divi%5Dslidernavigation/%5Bh-divi%5Dslider-dot-3dflip.css)
 - Fall effect : [Click Here](https://gitlab.com/jojaafar/harimau99-divi/blob/master/h-divi/%5Bh-divi%5Dslidernavigation/%5Bh-divi%5Dslider-dot-falleffect.css)
 - Puff effect : [Click Here](https://gitlab.com/jojaafar/harimau99-divi/blob/master/h-divi/%5Bh-divi%5Dslidernavigation/%5Bh-divi%5Dslider-dot-puffeffect.css)
 - Small dot with stroke : [Click Here](https://gitlab.com/jojaafar/harimau99-divi/blob/master/h-divi/%5Bh-divi%5Dslidernavigation/%5Bh-divi%5Dslider-dot-smalldotwithstroke.css)